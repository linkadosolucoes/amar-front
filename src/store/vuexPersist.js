import VuexPersist from "vuex-persist";

const vuexPersistSettings = new VuexPersist({
  key: "linkadoStorage",
  storage: window.localStorage,
});

export default vuexPersistSettings.plugin;
