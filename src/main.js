import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;

Vue.component("ConfirmDeleteModal", () =>
  import("@/components/Confirm/Delete/Index")
);

Vue.component("ModalChangePassword", () =>
  import("@/components/User/ChangePasswordModal/Index")
);

Vue.component("Paginate", () => import("@/components/Paginate/Index"));
Vue.component("LoadingModal", () => import("@/components/Loading/Modal/Index"));

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
