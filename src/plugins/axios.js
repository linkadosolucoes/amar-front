import axios from "axios";
import store from "@/store/index.js";

const instanceAxios = axios.create({
  baseURL: `${process.env.VUE_APP_HOST_API}/${process.env.VUE_APP_VERSION_API}`,
  timeout: 10000,
  responseType: "json",
  responseEncoding: "utf8",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

instanceAxios.interceptors.request.use(
  (config) => {
    const access_token = store.getters["getAccessToken"];
    if (access_token) {
      config.headers.Authorization = `${access_token}`;
    }

    return config;
  },
  (er) => {
    return Promise.reject(er.message);
  }
);

export default instanceAxios;
