import Vue from "vue";
import Vuetify from "vuetify/lib";
import "@fortawesome/fontawesome-free/css/all.css";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#0B6775",
        secondary: "#596162",
      },
    },
  },

  icons: {
    iconfont: "fa",
  },
});
