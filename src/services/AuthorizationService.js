import store from "@/store/index.js";

export default (role, permission = null) => {
  const dataPermissions = store.getters["getPermissions"];

  if (permission === null) {
    return dataPermissions[role] !== undefined;
  } else {
    if (dataPermissions !== undefined) {
      return dataPermissions[role] === undefined
        ? false
        : dataPermissions[role].some((item) => {
            return item === permission;
          });
    }

    return false;
  }
};
