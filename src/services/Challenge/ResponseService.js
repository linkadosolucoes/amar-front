import ErrorService from "@/services/ErrorService";
import RequestService from "@/services/RequestService";

export default class ResponseService extends RequestService {
  constructor(baseEndpoint) {
    super(baseEndpoint);
  }

  storeOrUpdate = async (endpoint, params) => {
    try {
      const { data } = await this.postOrPut(endpoint, params);

      return data;
    } catch (exception) {
      throw ErrorService(exception);
    }
  };
}
